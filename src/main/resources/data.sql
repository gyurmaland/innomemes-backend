INSERT INTO role (id, name)
SELECT 1,'ROLE_USER' WHERE NOT EXISTS (SELECT id FROM role WHERE id = 1);
INSERT INTO project (id, name)
SELECT 1,'BICON' WHERE NOT EXISTS (SELECT id FROM project WHERE id = 1);
INSERT INTO project (id, name)
SELECT 2,'OSZ' WHERE NOT EXISTS (SELECT id FROM project WHERE id = 2);
INSERT INTO project (id, name)
SELECT 3,'CTA' WHERE NOT EXISTS (SELECT id FROM project WHERE id = 3);
INSERT INTO project (id, name)
SELECT 4,'GENAT' WHERE NOT EXISTS (SELECT id FROM project WHERE id = 4);
INSERT INTO city_office (id, name)
SELECT 1,'Eger' WHERE NOT EXISTS (SELECT id FROM city_office WHERE id = 1);
INSERT INTO city_office (id, name)
SELECT 2,'Budapest' WHERE NOT EXISTS (SELECT id FROM city_office WHERE id = 2);
INSERT INTO city_office (id, name)
SELECT 3,'Debrecen' WHERE NOT EXISTS (SELECT id FROM city_office WHERE id = 3);