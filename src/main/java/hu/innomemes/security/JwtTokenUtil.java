package hu.innomemes.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Clock;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClock;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.function.Function;

@Component
public class JwtTokenUtil implements Serializable {

    @Value("${app.jwtSecret}")
    String prop;

    @Value("${app.jwtExpirationInMs}")
    private int jwtExpirationInMs;

    private Clock clock = DefaultClock.INSTANCE;

    public Long getUserIdFromToken(String token) {
        if (token.startsWith("Bearer")) {
            return Long.parseLong(getClaimFromToken(Objects.requireNonNull(StringUtils.split(token, " "))[1], Claims::getSubject));
        }
        return  Long.parseLong(getClaimFromToken(token, Claims::getSubject));
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser()
                .setSigningKey(prop)
                .parseClaimsJws(token)
                .getBody();
    }

    public String refreshToken(String token) {
        Date now = new Date();
        final Date createdDate = clock.now();
        Date expiryDate = new Date(now.getTime() + jwtExpirationInMs);

        final Claims claims = getAllClaimsFromToken(token);
        claims.setIssuedAt(createdDate);
        claims.setExpiration(expiryDate);

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, prop)
                .compact();
    }
}
