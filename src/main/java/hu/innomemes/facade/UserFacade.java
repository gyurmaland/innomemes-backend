package hu.innomemes.facade;

import com.querydsl.core.Tuple;
import freemarker.template.TemplateException;
import hu.innomemes.email.EmailService;
import hu.innomemes.email.Mail;
import hu.innomemes.error.ResourceNotFoundException;
import hu.innomemes.model.CityOffice;
import hu.innomemes.model.Project;
import hu.innomemes.model.QUser;
import hu.innomemes.model.User;
import hu.innomemes.model.enums.RoleName;
import hu.innomemes.payload.auth.NewPasswordRequest;
import hu.innomemes.payload.auth.PasswordResetCodeGenerateRequest;
import hu.innomemes.payload.auth.SignUpRequest;
import hu.innomemes.payload.auth.UserResponse;
import hu.innomemes.payload.ranking.TopUserResponse;
import hu.innomemes.payload.user.UserPreferencesRequest;
import hu.innomemes.service.CityOfficeService;
import hu.innomemes.service.ProjectService;
import hu.innomemes.service.RoleService;
import hu.innomemes.service.UserService;
import hu.innomemes.util.KeyGeneration;
import hu.innomemes.util.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.querydsl.QSort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class UserFacade {

    @Autowired
    UserService userService;

    @Autowired
    EmailService emailService;

    @Autowired
    RoleService roleService;

    @Autowired
    CityOfficeService cityOfficeService;

    @Autowired
    ProjectService projectService;

    @Autowired
    PasswordEncoder passwordEncoder;

    public List<TopUserResponse> findTop3User() {
        List<Tuple> users = userService.findAll();
        TreeMap<String, Long> userRanks = new TreeMap<>();
        for (Tuple user : users) {
            userRanks.put((String) user.toArray()[1], (Long) user.toArray()[0]);
        }
        LinkedHashMap<String, Long> sortedMap =
                userRanks.entrySet().stream().
                        sorted(Map.Entry.comparingByValue()).
                        collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                                (e1, e2) -> e1, LinkedHashMap::new));
        List<TopUserResponse> topUserResponseList = new ArrayList<>();
        for (Map.Entry<String, Long> userRank : sortedMap.entrySet()) {
            TopUserResponse topUserResponse = new TopUserResponse();
            topUserResponse.setUserName(userRank.getKey());
            topUserResponse.setMemeLikeCount(userRank.getValue());
            topUserResponseList.add(topUserResponse);
        }
        return topUserResponseList;
    }

    public UserResponse getUserById(Long id) {
        User user = userService.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("User", "id", id)
        );
        UserResponse userResponse = ObjectMapperUtils.map(user, UserResponse.class);
        return userResponse;
    }

    public UserResponse getUserByUsername(String username) {
        User user = userService.findByUsename(username);
        if (user == null) {
            throw new ResourceNotFoundException("User", "username", username);
        }
        UserResponse userResponse = ObjectMapperUtils.map(user, UserResponse.class);
        return userResponse;
    }

    public boolean existsByUsername(String username) {
        return userService.existsByUsername(username);
    }

    public boolean existsByEmail(String email) {
        return userService.existsByEmail(email);
    }

    public void registerUser(SignUpRequest signUpRequest) {
        User user = ObjectMapperUtils.map(signUpRequest, User.class);
        String activationKey = KeyGeneration.generateKey();
        user.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));
        user.setActivation(activationKey);
        user.setEnabled(false);
        user.setRoles(Collections.singleton(roleService.findByName(RoleName.ROLE_USER)));
        user.setCityOffices(new ArrayList<>());
        user.getCityOffices().addAll(cityOfficeService.findAllByNameIn(signUpRequest.getCityOfficeNames()));
        user.setProjects(new ArrayList<>());
        user.getProjects().addAll(projectService.findAllByNameIn(signUpRequest.getProjectNames()));
        userService.save(user);

        Mail mail = new Mail();
        mail.setFrom("markmytrainer@markmytrainer.com");
        mail.setTo(user.getEmail());
        mail.setSubject("InnoMemes Regisztráció Megerősítése");

        Map model = new HashMap();
        model.put("username", user.getUsername());
        //model.put("link", "https://localhost:8080/activation/" + activationKey);
        //model.put("link", "http://innomemes-frontend.s3-website.eu-central-1.amazonaws.com/activation/" + activationKey);
        //model.put("link", "https://mem-frontend.service.innovitech.internal/activation/" + activationKey);
        model.put("link", "https://mem.innovitech.hu/activation/" + activationKey);
        mail.setModel(model);

        try {
            emailService.sendSimpleMessage(mail);
        } catch (MessagingException | IOException | TemplateException e) {
            e.printStackTrace();
        }
    }

    public void activateUser(String activation) {
        User user = userService.findByActivation(activation);
        if (user == null) {
            throw new ResourceNotFoundException("User", "activation", activation);
        }
        user.setEnabled(true);
        user.setActivation("");
        userService.save(user);
    }

    public void generatePasswordResetCode(PasswordResetCodeGenerateRequest passwordResetCodeGenerateRequest) {
        User user = userService.findByEmail(passwordResetCodeGenerateRequest.getEmail());
        if (user == null) {
            throw new ResourceNotFoundException("User", "email", passwordResetCodeGenerateRequest.getEmail());
        }
        String passwordResetToken = userService.generatePasswordResetToken(user);

        Mail mail = new Mail();
        mail.setFrom("markmytrainer@markmytrainer.com");
        mail.setTo(user.getEmail());
        mail.setSubject("InnoMemes Elfelejtett Jelszó");

        Map model = new HashMap();
        model.put("username", user.getUsername());
        //model.put("link", "https://localhost:8080/passwordReset/" + passwordResetToken);
        //model.put("link", "http://innomemes-frontend.s3-website.eu-central-1.amazonaws.com/passwordReset/" + passwordResetToken);
        //model.put("link", "https://mem-frontend.service.innovitech.internal/passwordReset/" + passwordResetToken);
        model.put("link", "https://mem.innovitech.hu/newPassword/" + passwordResetToken);
        mail.setModel(model);

        try {
            emailService.sendPasswordResetEmail(mail);
        } catch (MessagingException | IOException | TemplateException e) {
            e.printStackTrace();
        }
    }

    public boolean userExistsByPwResetCode(String code) {
        if (!userService.existsByPwResetCode(code)) {
            throw new ResourceNotFoundException("User", "passwordResetCode", code);
        }
        return true;

    }

    public void findUserByPasswordResetCodeAndUpdatePassword(String code, NewPasswordRequest newPasswordRequest) {
        User user = userService.findByPasswordResetCode(code);
        if (user == null || user.getPasswordResetCode().isEmpty()) {
            throw new ResourceNotFoundException("User", "passwordResetCode", code);
        }
        userService.updatePassword(newPasswordRequest.getPassword(), user.getId());
    }

    public void updatePreferences(UserPreferencesRequest userPreferencesRequest) {
        User user = userService.findById(userPreferencesRequest.getUserId()).orElseThrow(
                () -> new ResourceNotFoundException("User", "id", userPreferencesRequest.getUserId())
        );
        List<CityOffice> cityOfficeList = cityOfficeService.findAllByNameIn(userPreferencesRequest.getCityOfficeNames());
        List<Project> projectList = projectService.findAllByNameIn(userPreferencesRequest.getProjectNames());
        user.setCityOffices(cityOfficeList);
        user.setProjects(projectList);
        userService.save(user);
    }
}
