package hu.innomemes.facade;

import hu.innomemes.error.ResourceNotFoundException;
import hu.innomemes.model.AppLike;
import hu.innomemes.model.Meme;
import hu.innomemes.model.User;
import hu.innomemes.payload.like.LikeRequest;
import hu.innomemes.payload.like.LikeResponse;
import hu.innomemes.service.LikeService;
import hu.innomemes.service.MemeService;
import hu.innomemes.service.UserService;
import hu.innomemes.util.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Component
public class LikeFacade {

    @Autowired
    LikeService likeService;

    @Autowired
    MemeService memeService;

    @Autowired
    UserService userService;

    public List<LikeResponse> findAllByMemeId(Long memeId) {
        List<AppLike> appLikeList = likeService.findAllByMemeId(memeId);
        List<LikeResponse> likeResponseList = ObjectMapperUtils.mapAll(appLikeList, LikeResponse.class);
        return likeResponseList;
    }

    public boolean saveOrDeleteLike(LikeRequest likeRequest) {
        Meme likedMeme = memeService.findById(likeRequest.getMemeId()).orElseThrow(
                () -> new ResourceNotFoundException("Meme", "id", likeRequest.getMemeId()));
        User likeUser = userService.findById(likeRequest.getUserId()).orElseThrow(
                () -> new ResourceNotFoundException("User", "id", likeRequest.getUserId()));
        AppLike appLike = likeService.findByMemeAndUser(likedMeme, likeUser);
        if (Objects.nonNull(appLike)) {
            likeService.delete(appLike);
            return false;
        }
        AppLike like = new AppLike();
        like.setMeme(likedMeme);
        like.setUser(likeUser);
        likeService.save(like);
        return true;
    }
}
