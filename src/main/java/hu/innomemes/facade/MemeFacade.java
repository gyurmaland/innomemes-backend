package hu.innomemes.facade;

import com.querydsl.core.types.OrderSpecifier;
import hu.innomemes.error.ResourceNotFoundException;
import hu.innomemes.model.Meme;
import hu.innomemes.model.QMeme;
import hu.innomemes.payload.meme.MemeListRequest;
import hu.innomemes.payload.meme.MemeListSearchRequest;
import hu.innomemes.payload.meme.MemeRequest;
import hu.innomemes.payload.meme.MemeResponse;
import hu.innomemes.service.CityOfficeService;
import hu.innomemes.service.MemeService;
import hu.innomemes.service.ProjectService;
import hu.innomemes.service.UserService;
import hu.innomemes.util.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.data.querydsl.QSort;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class MemeFacade {

    @Autowired
    MemeService memeService;

    @Autowired
    UserService userService;

    @Autowired
    ProjectService projectService;

    @Autowired
    CityOfficeService cityOfficeService;

    public Page<MemeResponse> findAllMeme(MemeListRequest memeListRequest) {
        QSort qSort = new QSort(QMeme.meme.createdDate.desc());
        if (Objects.nonNull(memeListRequest.getOrderBy())) {
            if (memeListRequest.getOrderBy().equals("likeNumber")) {
                qSort = new QSort(QMeme.meme.likes.size().desc());
            } else if (memeListRequest.getOrderBy().equals("createdDate")) {
                qSort = new QSort(QMeme.meme.createdDate.desc());
            }
        }
        QPageRequest qPageRequest = new QPageRequest(memeListRequest.getMemeListPagingRequest().getPage(),
                        memeListRequest.getMemeListPagingRequest().getPageSize(),
                        qSort);
        MemeListSearchRequest memeListSearchRequest = memeListRequest.getMemeListSearchRequest();
        Page<Meme> memePage = memeService.findAll(memeListSearchRequest, qPageRequest);
        List<MemeResponse> memeResponseList =
                ObjectMapperUtils.mapAll(memePage.getContent(), MemeResponse.class);
        return new PageImpl<>(memeResponseList, PageRequest
                .of(memeListRequest.getMemeListPagingRequest().getPage(),
                        memeListRequest.getMemeListPagingRequest().getPageSize()), memePage.getTotalElements());
    }

    public MemeResponse findMemeById(Long id) {
        Meme meme = memeService.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Meme", "id", id)
        );
        MemeResponse memeResponse = ObjectMapperUtils.map(meme, MemeResponse.class);
        return memeResponse;
    }

    public void deleteById(Long id) {
        memeService.deleteById(id);
    }

    public Long saveMeme(MemeRequest memeRequest) {
        Meme meme = ObjectMapperUtils.map(memeRequest, Meme.class);
        meme.setUser(userService.findById(memeRequest.getUserId()).orElse(null));
        meme.setProjects(new ArrayList<>());
        meme.getProjects().addAll(projectService.findAllByNameIn(memeRequest.getProjectNames()));
        meme.setCityOffices(new ArrayList<>());
        meme.getCityOffices().addAll(cityOfficeService.findAllByNameIn(memeRequest.getCityOfficeNames()));
        memeService.save(meme);
        return meme.getId();
    }
}
