package hu.innomemes.facade;

import hu.innomemes.model.CityOffice;
import hu.innomemes.service.CityOfficeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CityOfficeFacade {

    @Autowired
    CityOfficeService cityOfficeService;

    public void saveCityOffice(String cityOfficeName) {
        CityOffice cityOffice = new CityOffice();
        cityOffice.setName(cityOfficeName);
        cityOfficeService.saveCityOffice(cityOffice);
    }
}
