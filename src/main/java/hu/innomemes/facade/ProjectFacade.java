package hu.innomemes.facade;

import hu.innomemes.model.Project;
import hu.innomemes.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProjectFacade {

    @Autowired
    ProjectService projectService;

    public void saveProject(String projectName) {
        Project project = new Project();
        project.setName(projectName);
        projectService.saveProject(project);
    }

    public void deleteProject(Long projectId) {
        projectService.deleteProjectById(projectId);
    }
}
