package hu.innomemes.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Meme extends Auditable<String> {

    private String name;

    private String picUrl;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    private Boolean isProjectNotSpecified;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "Meme_Project",
            joinColumns = {@JoinColumn(name = "meme_id")},
            inverseJoinColumns = {@JoinColumn(name = "project_id")}
    )
    private List<Project> projects;

    private Boolean isCityOfficeNotSpecified;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "Meme_CityOffice",
            joinColumns = {@JoinColumn(name = "meme_id")},
            inverseJoinColumns = {@JoinColumn(name = "cityoffice_id")}
    )
    private List<CityOffice> cityOffices;

    @OneToMany(mappedBy = "meme", fetch = FetchType.LAZY)
    private List<AppLike> likes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isProjectNotSpecified() {
        return isProjectNotSpecified;
    }

    public void setProjectNotSpecified(boolean projectNotSpecified) {
        isProjectNotSpecified = projectNotSpecified;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public boolean isCityOfficeNotSpecified() {
        return isCityOfficeNotSpecified;
    }

    public void setCityOfficeNotSpecified(boolean cityOfficeNotSpecified) {
        isCityOfficeNotSpecified = cityOfficeNotSpecified;
    }

    public List<CityOffice> getCityOffices() {
        return cityOffices;
    }

    public void setCityOffices(List<CityOffice> cityOffices) {
        this.cityOffices = cityOffices;
    }

    public List<AppLike> getLikes() {
        return likes;
    }

    public void setLikes(List<AppLike> likes) {
        this.likes = likes;
    }
}
