package hu.innomemes.repository;

import hu.innomemes.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProjectRepository extends JpaRepository<Project, Long> {

    List<Project> findAllByNameIn(List<String> projectNames);
}
