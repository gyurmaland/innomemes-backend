package hu.innomemes.repository;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import hu.innomemes.model.QMeme;
import hu.innomemes.payload.meme.MemeListSearchRequest;

import java.util.Objects;

public class MemePredicate {
    private MemePredicate() {
    }

    public static Predicate advancedMemeSearch(MemeListSearchRequest memeListSearchRequest) {
        BooleanBuilder where = new BooleanBuilder();

        if (Objects.nonNull(memeListSearchRequest.getUniversalSearchStr()) && !memeListSearchRequest.getUniversalSearchStr().trim().isEmpty()) {
            where.or(QMeme.meme.name.containsIgnoreCase(memeListSearchRequest.getUniversalSearchStr().trim()))
            .or(QMeme.meme.user.username.containsIgnoreCase(memeListSearchRequest.getUniversalSearchStr().trim()))
            .or(QMeme.meme.user.fullName.containsIgnoreCase(memeListSearchRequest.getUniversalSearchStr().trim()))
            .or(QMeme.meme.projects.any().name.containsIgnoreCase(memeListSearchRequest.getUniversalSearchStr().trim()))
            .or(QMeme.meme.cityOffices.any().name.containsIgnoreCase(memeListSearchRequest.getUniversalSearchStr().trim()));
        }

        if (memeListSearchRequest.getIsNotSpecificProject()) {
            where.and(QMeme.meme.projects.isEmpty());
        }

        if (Objects.nonNull(memeListSearchRequest.getProjectNames()) && !memeListSearchRequest.getProjectNames().isEmpty()) {
            where.and(QMeme.meme.projects.any().name.in(memeListSearchRequest.getProjectNames()));
        }

        if (memeListSearchRequest.getIsNotSpecificCityOffice()) {
            where.and(QMeme.meme.cityOffices.isEmpty());
        }

        if (Objects.nonNull(memeListSearchRequest.getCityOfficeNames()) && !memeListSearchRequest.getCityOfficeNames().isEmpty()) {
            where.and(QMeme.meme.cityOffices.any().name.in(memeListSearchRequest.getCityOfficeNames()));
        }

        return where;
    }
}
