package hu.innomemes.repository;

import hu.innomemes.model.CityOffice;
import hu.innomemes.model.Project;
import hu.innomemes.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long>, QuerydslPredicateExecutor<User> {

    User findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    User findByEmail(String email);

    User findByActivation(String code);

    @Modifying
    @Query("update User u set u.passwordResetCode = :passwordResetCode where u.id = :id")
    @Transactional
    void updatePasswordResetToken(@Param("passwordResetCode") String passwordResetCode, @Param("id") Long id);

    boolean existsByPasswordResetCode(String code);

    User findByPasswordResetCode(String code);

    @Modifying
    @Query("update User u set u.password = :password, u.passwordResetCode = '' where u.id = :id")
    @Transactional
    void updatePassword(@Param("password") String password, @Param("id") Long id);

    @Modifying
    @Query("update User u set u.cityOffices = :cityOfficeList, u.projects = :projectList where u.id = :id")
    @Transactional
    void updatePreferences(@Param("cityOfficeList") List<CityOffice> cityOfficeList, @Param("projectList") List<Project> projectList, @Param("id") long userId);
}
