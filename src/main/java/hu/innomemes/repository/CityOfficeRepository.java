package hu.innomemes.repository;

import hu.innomemes.model.CityOffice;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CityOfficeRepository extends JpaRepository<CityOffice, Long> {

    List<CityOffice> findAllByNameIn(List<String> names);
}
