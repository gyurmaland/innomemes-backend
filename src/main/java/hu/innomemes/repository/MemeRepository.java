package hu.innomemes.repository;

import hu.innomemes.model.Meme;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface MemeRepository extends JpaRepository<Meme, Long>, QuerydslPredicateExecutor<Meme> {
}
