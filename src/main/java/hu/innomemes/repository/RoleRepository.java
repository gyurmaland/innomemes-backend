package hu.innomemes.repository;

import hu.innomemes.model.Role;
import hu.innomemes.model.enums.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByName(RoleName roleName);
}
