package hu.innomemes.repository;

import hu.innomemes.model.AppLike;
import hu.innomemes.model.Meme;
import hu.innomemes.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LikeRepository extends JpaRepository<AppLike, Long> {

    List<AppLike> findAllByMemeId(Long id);

    AppLike findByMemeAndUser(Meme meme, User user);
}
