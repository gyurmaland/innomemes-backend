package hu.innomemes.service;

import hu.innomemes.model.Project;
import hu.innomemes.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectService {

    @Autowired
    ProjectRepository projectRepository;

    public void saveProject(Project project) {
        projectRepository.save(project);
    }

    public void deleteProjectById(Long id) {
        projectRepository.deleteById(id);
    }

    public List<Project> findAllByIds(List<Long> projectIds) {
        return projectRepository.findAllById(projectIds);
    }

    public List<Project> findAllByNameIn(List<String> projectNames) {
        return projectRepository.findAllByNameIn(projectNames);
    }
}
