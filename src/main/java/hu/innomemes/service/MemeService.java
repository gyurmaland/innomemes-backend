package hu.innomemes.service;

import hu.innomemes.model.Meme;
import hu.innomemes.payload.meme.MemeListSearchRequest;
import hu.innomemes.repository.MemePredicate;
import hu.innomemes.repository.MemeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MemeService {

    @Autowired
    MemeRepository memeRepository;

    public Page<Meme> findAll(MemeListSearchRequest memeListSearchRequest, QPageRequest qPageRequest) {
        return memeRepository
                .findAll(MemePredicate.advancedMemeSearch(memeListSearchRequest), qPageRequest);
    }

    public Optional<Meme> findById(Long id) {
        return memeRepository.findById(id);
    }

    public void deleteById(Long id) {
        memeRepository.deleteById(id);
    }

    public void save(Meme meme) {
        memeRepository.save(meme);
    }
}
