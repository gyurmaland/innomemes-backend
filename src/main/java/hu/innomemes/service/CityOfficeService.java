package hu.innomemes.service;

import hu.innomemes.model.CityOffice;
import hu.innomemes.repository.CityOfficeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityOfficeService {

    @Autowired
    CityOfficeRepository cityOfficeRepository;

    public void saveCityOffice(CityOffice cityOffice) {
        cityOfficeRepository.save(cityOffice);
    }

    public List<CityOffice> findAllByIds(List<Long> cityOfficeIds) {
        return cityOfficeRepository.findAllById(cityOfficeIds);
    }

    public List<CityOffice> findAllByNameIn(List<String> cityOfficeNames) {
        return cityOfficeRepository.findAllByNameIn(cityOfficeNames);
    }
}
