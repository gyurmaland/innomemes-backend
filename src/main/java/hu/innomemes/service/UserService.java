package hu.innomemes.service;

import com.querydsl.core.Tuple;
import com.querydsl.jpa.impl.JPAQuery;
import hu.innomemes.model.CityOffice;
import hu.innomemes.model.Project;
import hu.innomemes.model.QAppLike;
import hu.innomemes.model.User;
import hu.innomemes.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public List<Tuple> findAll() {
        JPAQuery jpaQuery = new JPAQuery(em);
        hu.innomemes.model.QAppLike appLike = QAppLike.appLike;
        jpaQuery.select(appLike.id.count(), appLike.meme.user.username)
                .from(appLike)
                .groupBy(appLike.meme.user.username)
                .orderBy(appLike.id.count().desc())
                .limit(3);
        List<Tuple> top3 = jpaQuery.fetch();
        return top3;
    }

    public Optional<User> findById(Long userId) {
        return userRepository.findById(userId);
    }

    public User findByUsename(String username) {
        return userRepository.findByUsername(username);
    }

    public boolean existsByUsername(String username) {
        return userRepository.existsByUsername(username);
    }

    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User findByActivation(String activation) {
        return userRepository.findByActivation(activation);
    }

    public String generatePasswordResetToken(User user) {
        String passwordResetToken = UUID.randomUUID().toString();
        userRepository.updatePasswordResetToken(passwordResetToken, user.getId());
        return passwordResetToken;
    }

    public boolean existsByPwResetCode(String code) {
        return userRepository.existsByPasswordResetCode(code);
    }

    public User findByPasswordResetCode(String code) {
        return userRepository.findByPasswordResetCode(code);
    }

    public void updatePassword(String password, Long userId) {
        String encodedPassword = passwordEncoder.encode(password);
        userRepository.updatePassword(encodedPassword, userId);
        //asdasd
    }

    public void save(User user) {
        userRepository.save(user);
    }

    public void updatePreferences(List<CityOffice> cityOfficeList, List<Project> projectList, Long userId) {
        userRepository.updatePreferences(cityOfficeList, projectList, userId);
    }
}
