package hu.innomemes.service;

import hu.innomemes.model.AppLike;
import hu.innomemes.model.Meme;
import hu.innomemes.model.User;
import hu.innomemes.repository.LikeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LikeService {

    @Autowired
    LikeRepository likeRepository;

    public List<AppLike> findAllByMemeId(Long id) {
        return likeRepository.findAllByMemeId(id);
    }

    public AppLike findByMemeAndUser(Meme meme, User user) {
        return likeRepository.findByMemeAndUser(meme, user);
    }

    public void save(AppLike like) {
        likeRepository.save(like);
    }

    public void delete(AppLike like) {
        likeRepository.delete(like);
    }
}
