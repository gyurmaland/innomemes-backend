package hu.innomemes.service;

import hu.innomemes.model.Role;
import hu.innomemes.model.enums.RoleName;
import hu.innomemes.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

    @Autowired
    RoleRepository roleRepository;

    public Role findByName(RoleName roleName) {
        return roleRepository.findByName(roleName);
    }
}
