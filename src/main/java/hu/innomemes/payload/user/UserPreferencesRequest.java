package hu.innomemes.payload.user;

import hu.innomemes.model.CityOffice;
import hu.innomemes.model.Project;

import java.util.List;

public class UserPreferencesRequest {

    private List<String> cityOfficeNames;

    private List<String> projectNames;

    private long userId;

    public List<String> getCityOfficeNames() {
        return cityOfficeNames;
    }

    public void setCityOfficeNames(List<String> cityOfficeNames) {
        this.cityOfficeNames = cityOfficeNames;
    }

    public List<String> getProjectNames() {
        return projectNames;
    }

    public void setProjectNames(List<String> projectNames) {
        this.projectNames = projectNames;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
