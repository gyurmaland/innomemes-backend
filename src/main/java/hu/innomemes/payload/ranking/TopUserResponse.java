package hu.innomemes.payload.ranking;

public class TopUserResponse {

    private String userName;

    private Long memeLikeCount;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getMemeLikeCount() {
        return memeLikeCount;
    }

    public void setMemeLikeCount(Long memeLikeCount) {
        this.memeLikeCount = memeLikeCount;
    }
}
