package hu.innomemes.payload.meme;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

public class MemeRequest {

    @NotBlank
    @Size(min = 3, max = 40)
    private String name;

    @NotBlank
    private String picUrl;

    private Long userId;

    private List<String> projectNames;

    private List<String> cityOfficeNames;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<String> getProjectNames() {
        return projectNames;
    }

    public void setProjectNames(List<String> projectNames) {
        this.projectNames = projectNames;
    }

    public List<String> getCityOfficeNames() {
        return cityOfficeNames;
    }

    public void setCityOfficeNames(List<String> cityOfficeNames) {
        this.cityOfficeNames = cityOfficeNames;
    }
}
