package hu.innomemes.payload.meme;

import hu.innomemes.payload.cityoffice.CityOfficeResponse;
import hu.innomemes.payload.like.LikeResponse;
import hu.innomemes.payload.project.ProjectResponse;
import hu.innomemes.payload.user.UserResponse;

import java.util.List;

public class MemeResponse {

    private Long id;

    private String name;

    private String picUrl;

    private UserResponse user;

    private List<ProjectResponse> projects;

    private List<CityOfficeResponse> cityOffices;

    private List<LikeResponse> likes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public UserResponse getUser() {
        return user;
    }

    public void setUser(UserResponse user) {
        this.user = user;
    }

    public List<ProjectResponse> getProjects() {
        return projects;
    }

    public void setProjects(List<ProjectResponse> projects) {
        this.projects = projects;
    }

    public List<CityOfficeResponse> getCityOffices() {
        return cityOffices;
    }

    public void setCityOffices(List<CityOfficeResponse> cityOffices) {
        this.cityOffices = cityOffices;
    }

    public List<LikeResponse> getLikes() {
        return likes;
    }

    public void setLikes(List<LikeResponse> likes) {
        this.likes = likes;
    }
}
