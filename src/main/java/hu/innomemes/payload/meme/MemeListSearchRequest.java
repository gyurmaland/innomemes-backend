package hu.innomemes.payload.meme;

import java.util.List;

public class MemeListSearchRequest {

    private String universalSearchStr;

    private boolean isNotSpecificProject;

    private List<String> projectNames;

    private boolean isNotSpecificCityOffice;

    private List<String> cityOfficeNames;

    public String getUniversalSearchStr() {
        return universalSearchStr;
    }

    public void setUniversalSearchStr(String universalSearchStr) {
        this.universalSearchStr = universalSearchStr;
    }

    public boolean getIsNotSpecificProject() {
        return isNotSpecificProject;
    }

    public void setIsNotSpecificProject(boolean isNotSpecificProject) {
        this.isNotSpecificProject = isNotSpecificProject;
    }

    public List<String> getProjectNames() {
        return projectNames;
    }

    public void setProjectNames(List<String> projectNames) {
        this.projectNames = projectNames;
    }

    public boolean getIsNotSpecificCityOffice() {
        return isNotSpecificCityOffice;
    }

    public void setIsNotSpecificCityOffice(boolean isNotSpecificCityOffice) {
        this.isNotSpecificCityOffice = isNotSpecificCityOffice;
    }

    public List<String> getCityOfficeNames() {
        return cityOfficeNames;
    }

    public void setCityOfficeNames(List<String> cityOfficeNames) {
        this.cityOfficeNames = cityOfficeNames;
    }
}
