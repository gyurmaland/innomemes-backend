package hu.innomemes.payload.meme;

public class MemeListRequest {

    private MemeListPagingRequest memeListPagingRequest;

    private MemeListSearchRequest memeListSearchRequest;

    private String orderBy;

    public MemeListPagingRequest getMemeListPagingRequest() {
        return memeListPagingRequest;
    }

    public void setMemeListPagingRequest(MemeListPagingRequest memeListPagingRequest) {
        this.memeListPagingRequest = memeListPagingRequest;
    }

    public MemeListSearchRequest getMemeListSearchRequest() {
        return memeListSearchRequest;
    }

    public void setMemeListSearchRequest(MemeListSearchRequest memeListSearchRequest) {
        this.memeListSearchRequest = memeListSearchRequest;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }
}
