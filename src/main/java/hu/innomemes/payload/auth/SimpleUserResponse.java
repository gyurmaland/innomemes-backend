package hu.innomemes.payload.auth;

public class SimpleUserResponse {

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
