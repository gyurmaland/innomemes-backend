package hu.innomemes.payload.auth;

public class UserRequest {

    String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
