package hu.innomemes.payload.auth;

import hu.innomemes.payload.cityoffice.CityOfficeResponse;
import hu.innomemes.payload.project.ProjectResponse;

import java.util.List;

public class UserResponse {

    private Long id;

    private String fullName;

    private String username;

    private String email;

    private boolean enabled;

    private List<CityOfficeResponse> cityOffices;

    private List<ProjectResponse> projects;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public List<CityOfficeResponse> getCityOffices() {
        return cityOffices;
    }

    public void setCityOffices(List<CityOfficeResponse> cityOffices) {
        this.cityOffices = cityOffices;
    }

    public List<ProjectResponse> getProjects() {
        return projects;
    }

    public void setProjects(List<ProjectResponse> projects) {
        this.projects = projects;
    }
}
