package hu.innomemes.payload.like;

import hu.innomemes.payload.auth.SimpleUserResponse;

public class LikeResponse {

    private SimpleUserResponse user;

    private String memeId;

    public SimpleUserResponse getUser() {
        return user;
    }

    public void setUser(SimpleUserResponse user) {
        this.user = user;
    }

    public String getMemeId() {
        return memeId;
    }

    public void setMemeId(String memeId) {
        this.memeId = memeId;
    }
}
