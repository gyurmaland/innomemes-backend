package hu.innomemes.payload.like;

public class LikeSearchRequest {

    private String memeId;

    public String getMemeId() {
        return memeId;
    }

    public void setMemeId(String memeId) {
        this.memeId = memeId;
    }
}
