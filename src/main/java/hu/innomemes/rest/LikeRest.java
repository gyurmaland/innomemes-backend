package hu.innomemes.rest;

import hu.innomemes.facade.LikeFacade;
import hu.innomemes.payload.like.LikeRequest;
import hu.innomemes.payload.like.LikeResponse;
import hu.innomemes.payload.like.LikeSearchRequest;
import hu.innomemes.security.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class LikeRest {

    @Autowired
    LikeFacade likeFacade;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @CrossOrigin
    @PostMapping("memelikes")
    public ResponseEntity memeLikes(@RequestBody @Valid LikeSearchRequest likeSearchRequest) {
        List<LikeResponse> likeResponseList = likeFacade.findAllByMemeId(Long.parseLong(likeSearchRequest.getMemeId()));
        return new ResponseEntity(likeResponseList, HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping("like/save")
    public ResponseEntity saveLike(@RequestHeader(value="Authorization") String token, @RequestBody @Valid LikeRequest likeRequest) {
        Long userId = jwtTokenUtil.getUserIdFromToken(token);
        likeRequest.setUserId(userId);
        boolean isLikeCreated = likeFacade.saveOrDeleteLike(likeRequest);
        return new ResponseEntity(isLikeCreated, HttpStatus.CREATED);
    }
}
