package hu.innomemes.rest;

import hu.innomemes.facade.UserFacade;
import hu.innomemes.payload.ApiResponse;
import hu.innomemes.payload.auth.*;
import hu.innomemes.security.JwtTokenProvider;
import hu.innomemes.security.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
public class AuthRest {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserFacade userFacade;

    @Autowired
    JwtTokenProvider tokenProvider;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @CrossOrigin
    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );

        UserResponse userResponse = userFacade.getUserByUsername(loginRequest.getUsername());

        if (!userResponse.isEnabled()) {
            return new ResponseEntity<>(new ApiResponse(false, "User not yet activated!"),
                    HttpStatus.NOT_FOUND);
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, userResponse));
    }

    @CrossOrigin
    @GetMapping("/activation/{code}")
    public ResponseEntity<?> activateUser(@PathVariable("code") String code) {
        userFacade.activateUser(code);
        return new ResponseEntity(HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping("/passwordReset/generateCode")
    public ResponseEntity<?> resetPassword(@Valid @RequestBody PasswordResetCodeGenerateRequest passwordResetCodeGenerateRequest) {
        userFacade.generatePasswordResetCode(passwordResetCodeGenerateRequest);
        return new ResponseEntity(HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/checkPwResetCode/{code}")
    public ResponseEntity<?> checkPwResetCode(@PathVariable("code") String code) {
        userFacade.userExistsByPwResetCode(code);
        return ResponseEntity.ok(userFacade.userExistsByPwResetCode(code));
    }

    @CrossOrigin
    @PostMapping("/passwordReset/{code}")
    public ResponseEntity<?> resetPassword(@PathVariable("code") String code, @Valid @RequestBody NewPasswordRequest newPasswordRequest) {
        userFacade.findUserByPasswordResetCodeAndUpdatePassword(code, newPasswordRequest);
        return new ResponseEntity(HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        if (userFacade.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity<>(new ApiResponse(false, "Username is already taken!"),
                    HttpStatus.BAD_REQUEST);
        }
        if (userFacade.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity<>(new ApiResponse(false, "Email Address already in use!"),
                    HttpStatus.BAD_REQUEST);
        }

        userFacade.registerUser(signUpRequest);

        return new ResponseEntity(HttpStatus.CREATED);
    }

    @CrossOrigin
    @GetMapping("/isUsernameAlreadyTaken/{username}")
    public ResponseEntity<?> isUsernameAlreadyTaken(@PathVariable("username") String username) {
        if (userFacade.existsByUsername(username)) {
            return new ResponseEntity<>(true, HttpStatus.OK);
        }
        return new ResponseEntity<>(false, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/isEmailAlreadyTaken/{email}")
    public ResponseEntity<?> isEmailAlreadyTaken(@PathVariable("email") String email) {
        if (userFacade.existsByEmail(email)) {
            return new ResponseEntity<>(true, HttpStatus.OK);
        }
        return new ResponseEntity<>(false, HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping("/loggedInUser")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody UserRequest userRequest) {
        Long userId = jwtTokenUtil.getUserIdFromToken(userRequest.getToken());
        UserResponse userResponse = userFacade.getUserById(userId);
        return new ResponseEntity<>(userResponse, HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/refreshToken", method = RequestMethod.GET)
    public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
        String authToken = request.getHeader("Authorization");
        final String token = authToken.substring(7);

        String refreshedToken = jwtTokenUtil.refreshToken(token);
        return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken));

    }
}
