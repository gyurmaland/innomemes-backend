package hu.innomemes.rest;

import hu.innomemes.facade.UserFacade;
import hu.innomemes.payload.user.UserPreferencesRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
public class UserRest {

    @Autowired
    UserFacade userFacade;

    @CrossOrigin
    @GetMapping("/top3user")
    public ResponseEntity findTop3User() {
        return new ResponseEntity(userFacade.findTop3User(), HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping("/updatePreferences")
    public ResponseEntity<?> updateUserPreferences(@Valid @RequestBody UserPreferencesRequest userPreferencesRequest) {
        userFacade.updatePreferences(userPreferencesRequest);
        return new ResponseEntity(HttpStatus.CREATED);
    }
}
