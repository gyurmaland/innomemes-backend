package hu.innomemes.rest;

import hu.innomemes.facade.CityOfficeFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class CityOfficeRest {

    @Autowired
    CityOfficeFacade cityOfficeFacade;

    @CrossOrigin
    @PostMapping("/cityOffice/save")
    public ResponseEntity saveCityOffice(@RequestHeader(value = "Authorization") String token, @RequestBody @Valid String cityOfficeName) {
        cityOfficeFacade.saveCityOffice(cityOfficeName);
        return new ResponseEntity(HttpStatus.CREATED);
    }
}
