package hu.innomemes.rest;

import hu.innomemes.facade.MemeFacade;
import hu.innomemes.facade.UserFacade;
import hu.innomemes.payload.meme.MemeListRequest;
import hu.innomemes.payload.meme.MemeRequest;
import hu.innomemes.security.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class MemeRest {

    @Autowired
    MemeFacade memeFacade;

    @Autowired
    UserFacade userFacade;


    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @CrossOrigin
    @PostMapping("/meme/save")
    public ResponseEntity saveMeme(@RequestHeader(value = "Authorization") String token, @RequestBody @Valid MemeRequest memeRequest) {
        Long userId = jwtTokenUtil.getUserIdFromToken(token);
        memeRequest.setUserId(userId);
        Long memeId = memeFacade.saveMeme(memeRequest);
        return new ResponseEntity(memeId, HttpStatus.CREATED);
    }

    @CrossOrigin
    @GetMapping("/meme/delete/{memeId}")
    public ResponseEntity saveMeme(@PathVariable Long memeId) {
        memeFacade.deleteById(memeId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping("/memes/{memeId}")
    ResponseEntity findMemeById(@PathVariable Long memeId) {
        return new ResponseEntity(memeFacade.findMemeById(memeId), HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping("/memes")
    public ResponseEntity findAll(@RequestBody @Valid MemeListRequest memeListRequest) {
        return new ResponseEntity(memeFacade.findAllMeme(memeListRequest), HttpStatus.OK);
    }

}
