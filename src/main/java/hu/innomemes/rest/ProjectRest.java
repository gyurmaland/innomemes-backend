package hu.innomemes.rest;

import hu.innomemes.facade.ProjectFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ProjectRest {

    @Autowired
    ProjectFacade projectFacade;

    @CrossOrigin
    @PostMapping("/project/save")
    public ResponseEntity saveProject(@RequestHeader(value = "Authorization") String token, @RequestBody @Valid String projectName) {
        projectFacade.saveProject(projectName);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @CrossOrigin
    @GetMapping("/project/delete/{projectId}")
    public ResponseEntity saveProject(@RequestHeader(value = "Authorization") String token, @PathVariable Long projectId) {
        projectFacade.deleteProject(projectId);
        return new ResponseEntity(HttpStatus.OK);
    }
}
